const express = require('express');
const app = express();
const fs = require('fs');
const url = __dirname + '/data.json';
const models = require('./models');

app.get('/', function (req, res) {
  fs.readFile(url, 'utf8', function (err, data) {
    if (err) {
      console.log('Error: ' + err);
      return;
    }
    var objList = JSON.parse(data);
    objList.forEach(function (obj) {
      models.Obj.create({
        category: obj.category
      }).then(function () {
        console.log('saved');
      });
    });
  });
});

app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});