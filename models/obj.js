'use strict';
module.exports = (sequelize, DataTypes) => {
  var Obj = sequelize.define('Obj', {
    category: DataTypes.STRING
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });
  return Obj;
};